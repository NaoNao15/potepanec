require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "商品詳細ページ表示" do
    let!(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "@productが期待される値をもつ" do
      expect(assigns(:product)).to eq product
    end

    it "レスポンス成功" do
      expect(response).to be_successful
    end

    it "showページが表示される" do
      expect(response).to render_template :show
    end
  end
end
