require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:taxon) { create(:taxon, taxonomy: taxonomy) }
  given!(:taxonomy) { create(:taxonomy) }
  given!(:product) { create(:product, taxons: [taxon]) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario 'カテゴリーページから商品詳細ページへ移動' do
    expect(page).to have_link product.name
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario 'カテゴリーページに@productの情報があるか（商品名、商品価格）' do
    within '.productBox' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end
end
