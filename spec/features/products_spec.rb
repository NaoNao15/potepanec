require 'rails_helper'

RSpec.feature "Products", type: :feature do
  given(:product) { create(:product) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "商品情報の確認（商品名、値段、商品の概要）" do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end
end
